# jsonschema-maven-plugin #

A plugin for creating JSON schemas from Java objects during your Maven build.

### Maven configuration ###


```
#!xml

<build>
	<plugins>
		<plugin>
			<groupId>uk.co.wansdykehouse</groupId>
			<artifactId>jsonschema-maven-plugin</artifactId>
			<version>1.1</version>
			<executions>
				<execution>
					<phase>package</phase>
					<goals>
						<goal>jsonschema</goal>
					</goals>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```